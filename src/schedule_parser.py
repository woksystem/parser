#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import copy
import time
from http import HTTPStatus
import random
import requests
from bs4 import BeautifulSoup
from fake_useragent import UserAgent

from packages.misc import calc_md5_hash, json, read_json, write_file

DOMAIN = 'http://www.istu.edu'
SCHEDULE_URL = DOMAIN + '/schedule/'

SCHEDULE = {
  "schedule": {
    "period": "",
    "days": [
      {
        "name": "Понедельник",
        "isEven": True,
        "lessons": []
      },
      {
        "name": "Понедельник",
        "isEven": False,
        "lessons": []
      },
      {
        "name": "Вторник",
        "isEven": True,
        "lessons": []
      },
      {
        "name": "Вторник",
        "isEven": False,
        "lessons": []
      },
      {
        "name": "Среда",
        "isEven": True,
        "lessons": []
      },
      {
        "name": "Среда",
        "isEven": False,
        "lessons": []
      },
      {
        "name": "Четверг",
        "isEven": True,
        "lessons": []
      },
      {
        "name": "Четверг",
        "isEven": False,
        "lessons": []
      },
      {
        "name": "Пятница",
        "isEven": True,
        "lessons": []
      },
      {
        "name": "Пятница",
        "isEven": False,
        "lessons": []
      },
      {
        "name": "Суббота",
        "isEven": True,
        "lessons": []
      },
      {
        "name": "Суббота",
        "isEven": False,
        "lessons": []
      }
    ]
  }
}


def random_get(url):
    time.sleep(random.randint(1, 3))
    return requests.get(url, headers={'User-Agent': str(UserAgent().random)})


def get_groups(subdiv: str):
    response = random_get(SCHEDULE_URL + subdiv)
    if response.status_code != HTTPStatus.OK:
        return ()
    html = BeautifulSoup(response.text, 'html.parser')
    curs_list = html.find(attrs={'class': 'kurs-list'})
    if not curs_list:
        return ()
    for link in curs_list.find_all('a'):
        yield link.string, link['href']


def make_lesson(lesson: dict, time, elem):
    if elem.string and 'свободно' in elem.string:
        return lesson
    name = elem.find(attrs={'class-pred'}).string
    if name is None:
        name = ''
    location = elem.find(attrs={'class-aud'}).string
    if location is None:
        location = ''
    info = elem.find_all(limit=2, attrs={'class-info'})
    teacher = info[1].string
    if teacher is None:
        teacher = ''
    type = info[0].string.strip()[1:-1].strip()
    if type.endswith(','):
        type = type[0:-1]
    if 'подгруппа 1' in type:
        is_dual = True if lesson['s_type'] != '' else False
        lesson.update({
            'isDual': is_dual,
            'f_name': name,
            't_start': time,
            'f_type': type,
            'f_teacher': teacher,
            'f_location': location,
        })
    elif 'подгруппа 2' in type:
        is_dual = True if lesson['f_type'] != '' else False
        lesson.update({
            'isDual': is_dual,
            's_name': name,
            't_start': time,
            's_type': type,
            's_teacher': teacher,
            's_location': location
        })
    else:
        lesson.update({
            'isDual': False,
            'f_name': name,
            's_name': '',
            't_start': time,
            't_end': '',
            'f_type': type,
            's_type': '',
            'f_teacher': teacher,
            's_teacher': '',
            'f_location': location,
            's_location': ''
        })
    return lesson


def get_lesson(elem, div_class) -> dict:
    time = elem.find(attrs={'class': 'class-time'}).string
    lesson = {
        'isDual': False,
        'f_name': '',
        's_name': '',
        't_start': None,
        't_end': '',
        'f_type': '',
        's_type': '',
        'f_teacher': '',
        's_teacher': '',
        'f_location': '',
        's_location': ''
    }
    for lesson_div in elem.find_all(attrs={'class-tail class-all-week'}):
        try:
            lesson = make_lesson(lesson, time, lesson_div)
        except Exception as e:
            print(str(e))
    for lesson_div in elem.find_all(attrs={div_class}):
        try:
            lesson = make_lesson(lesson, time, lesson_div)
        except Exception as e:
            print(str(e))
    return lesson if lesson['t_start'] else None


def get_schedule(group: str):
    schedule_days = []

    response = random_get(SCHEDULE_URL + group)
    if response.status_code != HTTPStatus.OK:
        return schedule_days
    html = BeautifulSoup(response.text, 'html.parser')

    schedule_list = html.find(attrs={'class': 'full-even-week'})
    if not schedule_list:
        schedule_list = html.find(attrs={'class': 'full-odd-week'})
    if not schedule_list:
        return schedule_days

    week_days = schedule_list.find_all(attrs={'class': 'day-heading'})
    week_lessons = schedule_list.find_all(attrs={'class': 'class-lines'})

    for i, day in enumerate(week_days):
        try:
            day = day.string.split(',')[0].capitalize()
            even_day = {'name': day, 'isEven': True, 'lessons': []}
            odd_day = {'name': day, 'isEven': False, 'lessons': []}
            for class_tail in week_lessons[i].find_all(attrs={'class': 'class-tails'}):
                even_lesson = get_lesson(class_tail, 'class-tail class-even-week')
                odd_lesson = get_lesson(class_tail, 'class-tail class-odd-week')
                if even_lesson:
                    even_day['lessons'].append(even_lesson)
                if odd_lesson:
                    odd_day['lessons'].append(odd_lesson)
            schedule_days.append(even_day)
            schedule_days.append(odd_day)
        except Exception as e:
            print(str(e))
    return schedule_days


def main():
    response = random_get(SCHEDULE_URL)
    if response.status_code != HTTPStatus.OK:
        exit(1)

    html = BeautifulSoup(response.text, 'html.parser')
    content = html.find(attrs={'class': 'content'})
    if not content:
        exit(1)

    groups = []
    for subdiv in content.find_all('a'):
        for name, url in get_groups(subdiv['href']):
            try:
                groups.append(name)
                schedule_tmpl = copy.deepcopy(SCHEDULE)
                schedule_days = get_schedule(url)
                for i, day_tmpl in enumerate(schedule_tmpl['schedule']['days']):
                    schedule_day = next((day for day in schedule_days if day['name'] == day_tmpl['name'] and day['isEven'] == day_tmpl['isEven']), None)
                    if schedule_day:
                        schedule_tmpl['schedule']['days'][i] = schedule_day
                md5_hash = calc_md5_hash(json.dumps(schedule_tmpl, ensure_ascii=False).encode('utf8'))
                write_file('data/groups/{}.json'.format(name), json.dumps({'hash': md5_hash, 'schedule': schedule_tmpl['schedule']}, ensure_ascii=False))
            except Exception as e:
                print(str(e))

    md5_hash = calc_md5_hash(json.dumps(groups, ensure_ascii=False).encode('utf8'))
    write_file('data/groups.json', json.dumps({'hash': md5_hash, 'groups': groups}, ensure_ascii=False))


if __name__ == '__main__':
    main()
