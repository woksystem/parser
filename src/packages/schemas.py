NEWS_SCHEMA = {
    "type": "object",
    "properties": {
        "hash": {
            "type": "string"
        },
        "news": {
            "type": "array",
            "items": {
                "type": "object",
                "properties": {
                    "id": {
                        "type": "string"
                    },
                    "url": {
                        "type": "string"
                    },
                    "title": {
                        "type": "string"
                    },
                    "date": {
                        "type": "string"
                    },
                    "categories": {
                        "type": [
                            "array",
                            "null"
                        ],
                        "items": {
                            "type": "string"
                        }
                    },
                    "content": {
                        "type": "array",
                        "items": {
                            "type": "string"
                        }
                    },
                    "header_image": {
                        "type": "string"
                    },
                    "images": {
                        "type": [
                            "array",
                            "null"
                        ],
                        "items": {
                            "type": "string"
                        }
                    }
                },
                "required": [
                    "id",
                    "url",
                    "title",
                    "date",
                    "categories",
                    "content",
                    "header_image",
                    "images"
                ]
            }
        }
    },
    "required": [
        "hash",
        "news"
    ]
}

GROUPS_SCHEMA = {
    "type": "object",
    "properties": {
        "hash": {
            "type": "string"
        },
        "groups": {
            "type": "array",
            "items": {
                "type": "string"
            }
        }
    },
    "required": [
        "hash",
        "groups"
    ]
}

GROUP_SCHEDULE_SCHEMA = {
    "type": "object",
    "properties": {
        "hash": {
            "type": "string"
        },
        "schedule": {
            "type": "object",
            "properties": {
                "period": {
                    "type": "string"
                },
                "days": {
                    "type": "array",
                    "items": {
                        "type": "object",
                        "properties": {
                            "name": {
                                "type": "string"
                            },
                            "isEven": {
                                "type": "boolean"
                            },
                            "lessons": {
                                "type": "array",
                                "items": {
                                    "type": "object",
                                    "properties": {
                                        "isDual": {
                                            "type": "boolean"
                                        },
                                        "f_name": {
                                            "type": "string"
                                        },
                                        "s_name": {
                                            "type": "string"
                                        },
                                        "t_start": {
                                            "type": "string"
                                        },
                                        "t_end": {
                                            "type": "string"
                                        },
                                        "f_type": {
                                            "type": "string"
                                        },
                                        "s_type": {
                                            "type": "string"
                                        },
                                        "f_teacher": {
                                            "type": "string"
                                        },
                                        "s_teacher": {
                                            "type": "string"
                                        },
                                        "f_location": {
                                            "type": "string"
                                        },
                                        "s_location": {
                                            "type": "string"
                                        }
                                    },
                                    "required": [
                                        "isDual",
                                        "f_name",
                                        "s_name",
                                        "t_start",
                                        "t_end",
                                        "f_type",
                                        "s_type",
                                        "f_teacher",
                                        "s_teacher",
                                        "f_location",
                                        "s_location"
                                    ]
                                }
                            }
                        },
                        "required": [
                            "name",
                            "isEven",
                            "lessons"
                        ]
                    }
                }
            },
            "required": [
                "period",
                "days"
            ]
        }
    },
    "required": [
        "hash",
        "schedule"
    ]
}
