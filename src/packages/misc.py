import hashlib
import json
import os
from typing import List

from jsonschema import ValidationError, validate


def calc_md5_hash(text: str) -> str:
    return hashlib.md5(text).hexdigest()


def write_file(path: str, text: str):
    f = open(path, 'w')
    f.write(text)
    f.close()


def read_file(path: str) -> List[str]:
    if not os.path.exists(path):
        return None
    f = open(path, 'r+')
    lines = f.readlines()
    f.close()
    return lines if lines else None


def read_and_validate_json(path: str, schema: dict) -> dict:
    __json = read_json(path)
    return None if __json is None or not validate_json(__json, schema) else __json


def read_json(path: str) -> dict:
    if not os.path.exists(path):
        return None
    f = open(path, 'r+')
    data = f.read()
    f.close()
    if len(data) == 0 or data is None:
        return None
    try:
        return json.loads(data)
    except ValueError:
        return None


def validate_json(__json: dict, __schema: dict) -> bool:
    try:
        validate(instance=__json, schema=__schema)
        return True
    except ValidationError:
        return False
