#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import subprocess
import time
import urllib.parse
from enum import Enum
from http import HTTPStatus
from http.server import BaseHTTPRequestHandler, HTTPServer
from socketserver import ThreadingMixIn
from threading import Thread
from typing import Tuple

import schedule

from news_parser import PATH_TO_NEWS
from packages.misc import json, read_and_validate_json
from packages.schemas import GROUP_SCHEDULE_SCHEMA, GROUPS_SCHEMA, NEWS_SCHEMA

URL_SEP = '?'
SHEDULE_URL = 'http://www.istu.edu/schedule/export/schedule_exp.txt'
PATH_TO_GROUPS = 'data/groups'
PATH_TO_SCRIPTS = '/root/bin/'


def parse_url(url: str) -> Tuple[str, dict]:
    if URL_SEP not in url:
        return url, None

    splited_url = url.split(URL_SEP)
    if len(splited_url) > 2:
        return None, None

    url_endpoint = splited_url[0]
    url_params = urllib.parse.parse_qs(splited_url[1], True)
    return url_endpoint, url_params


def get_url_param(var: str, params: dict) -> str:
    return params[var][0] if var in params else None


def get_groups(__hash: str) -> bytes:
    groups = read_and_validate_json('{}.json'.format(PATH_TO_GROUPS), GROUPS_SCHEMA)
    if groups is None:
        return None

    if groups['hash'] == __hash:
        return json.dumps({}).encode()
    return json.dumps(
        {
            'hash': groups['hash'],
            'groups': groups['groups']
        }).encode()


def get_news() -> bytes:
    news = read_and_validate_json(PATH_TO_NEWS, NEWS_SCHEMA)
    if news is None:
        return None
    return json.dumps({'news': news['news'][:40]}).encode()


def get_group_schedule(__hash: str, group: str) -> bytes:
    group_schedule = read_and_validate_json('{}/{}.json'.format(PATH_TO_GROUPS, group), GROUP_SCHEDULE_SCHEMA)
    if group_schedule is None:
        return None

    if group_schedule['hash'] == __hash:
        return json.dumps({}).encode()
    return json.dumps(
        {
            'hash': group_schedule['hash'],
            'schedule': group_schedule['schedule']
        }).encode()


class UrlEndpoind(Enum):
    GROUPS = '/groups'
    SCHEDULE = '/schedule'
    NEWS = '/news'


class Handler(BaseHTTPRequestHandler):
    def do_GET(self):
        url_endpoint, url_params = parse_url(self.path)
        if url_endpoint == UrlEndpoind.GROUPS.value:
            self._handle_groups(url_params)
            return
        if url_endpoint == UrlEndpoind.SCHEDULE.value:
            self._handle_group_schedule(url_params)
            return
        if url_endpoint == UrlEndpoind.NEWS.value:
            self._handle_news()
            return

        self.send_error(HTTPStatus.BAD_REQUEST)


    def do_POST(self):
        self.send_error(HTTPStatus.BAD_REQUEST)

    def _handle_group_schedule(self, url_params: dict):
        if url_params is None:
            self.send_error(HTTPStatus.BAD_REQUEST)
            return

        req_hash = get_url_param('hash', url_params)
        req_group = get_url_param('group', url_params)

        if req_group is None:
            self.send_error(HTTPStatus.BAD_REQUEST)
            return

        group_schedule = get_group_schedule(req_hash, req_group)
        if group_schedule is None:
            self.send_error(HTTPStatus.INTERNAL_SERVER_ERROR)
            return

        self._set_success_headers()
        self.wfile.write(group_schedule)

    def _handle_groups(self, url_params: dict):
        req_hash = None if url_params is None else get_url_param('hash', url_params)
        groups = get_groups(req_hash)
        if groups is None:
            self.send_error(HTTPStatus.INTERNAL_SERVER_ERROR)
            return

        self._set_success_headers()
        self.wfile.write(groups)

    def _handle_news(self):
        news = get_news()
        if news is None:
            self.send_error(HTTPStatus.INTERNAL_SERVER_ERROR)
            return

        self._set_success_headers()
        self.wfile.write(news)

    def _set_success_headers(self):
        self.send_response(HTTPStatus.OK)
        self.send_header('Content-type', 'application/json')
        self.end_headers()


class ThreadingHTTPServer(ThreadingMixIn, HTTPServer):
    pass


def serve_on_port(host: str, port: int):
    server = ThreadingHTTPServer((host, port), Handler)
    server.serve_forever()


def update(job_name: str, script: str):
    timeout = time.time() + 3600
    print('Update {} start'.format(job_name))
    while time.time() < timeout:
        print('try {} update...'.format(job_name))
        if subprocess.Popen(['python3 {}'.format(script)], shell=True).wait() == 0:
            print('update {} done'.format(job_name))
            return
        time.sleep(100)
    print('update {} fail'.format(job_name))
    return


def run_update():
    while True:
        schedule.run_pending()
        time.sleep(1)


if __name__ == "__main__":
    schedule.every().day.at('01:00').do(update,
                                        job_name='schedule',
                                        script='{}schedule_parser.py'.format(PATH_TO_SCRIPTS))
    schedule.every().day.at('01:00').do(update,
                                        job_name='news',
                                        script='{}news_parser.py'.format(PATH_TO_SCRIPTS))

    Thread(target=update,
           args=['schedule', '{}schedule_parser.py'.format(PATH_TO_SCRIPTS)]).start()
    Thread(target=update,
           args=['news', '{}news_parser.py'.format(PATH_TO_SCRIPTS)]).start()

    Thread(target=serve_on_port, args=['', 8000]).start()
    Thread(target=run_update).start()
