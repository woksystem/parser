#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import multiprocessing as mp
import os
import re
from http import HTTPStatus

import requests
from bs4 import BeautifulSoup

from packages.misc import calc_md5_hash, json, read_json, write_file

DOMAIN = 'https://www.istu.edu'
NEWS_URL = DOMAIN + '/news/'
PATH_TO_NEWS = 'data/news.json'
RE_STR_NEWS = r'<h4><a href=\"/news/(\d+)/'
MAX_PAGE = 5
WORKERS = 20


def get_single_news(news_id: str) -> str:
    news_response = requests.get(NEWS_URL + news_id)
    if news_response.status_code != HTTPStatus.OK:
        return None

    html = BeautifulSoup(news_response.text, 'html.parser')
    if html.find(attrs={'class': 'alert alert-danger'}):
        return None

    title = html.find('h1').string
    date = html.find(attrs={'class': 'new-date'}).string
    categories = [tag.string for tag in html.find_all(attrs={'class': 'new-razdel-item'})]
    terms = html.find(attrs={'class': 'content news-content'}).text.split('\n')
    # Первые три строки: дата, теги слитные, заголовок
    content = [it.strip() for it in terms if it.strip()][3:]
    header_image = DOMAIN + html.find(attrs={'class': 'news-header'}).img['src']

    gallery = html.find(attrs={'class': 'img-gall'})
    images = None if gallery is None \
        else [DOMAIN + link['href'] for link in html.find(attrs={'class': 'img-gall'}).find_all('a')]

    path = "/tmp/news_{}".format(news_id)
    write_file(path, json.dumps({
        'id': news_id,
        'url': NEWS_URL + str(news_id),
        'title': title,
        'date': date,
        'categories': categories,
        'content': content,
        'header_image': header_image,
        'images': images}, ensure_ascii=False))
    return path


def main():
    response = requests.get(NEWS_URL)
    if response.status_code != HTTPStatus.OK:
        exit(1)

    old_news = read_json(PATH_TO_NEWS)
    if old_news and (re.search(RE_STR_NEWS, response.text).group(1) == old_news['news'][0]['id']):
        exit(0)

    pool = mp.Pool(processes=WORKERS)
    list_news = []
    for page_id in range(1, MAX_PAGE + 1):
        print('--- page {} ---'.format(page_id))
        page_response = requests.get(NEWS_URL, params={'PAGEN_1': page_id})
        if page_response.status_code != HTTPStatus.OK:
            continue

        results = pool.map(get_single_news, re.findall(RE_STR_NEWS, page_response.text))
        for tmp in results:
            if not tmp:
                continue

            tmp_json = read_json(tmp)
            if tmp_json:
                list_news.append(tmp_json)
            os.remove(tmp)

    md5_hash = calc_md5_hash(json.dumps({'news': list_news}, ensure_ascii=False).encode('utf-8'))
    write_file(PATH_TO_NEWS, json.dumps({'hash': md5_hash, 'news': list_news}, ensure_ascii=False))


if __name__ == '__main__':
    main()
