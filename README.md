# Сервер парсинга расписания istu.edu
[Файл(5 mb)](http://www.istu.edu/schedule/export/schedule_exp.txt) расписания
## Запуск на сервере
1. Установить [docker-ce](https://docs.docker.com/install/linux/docker-ce/debian/)
2. Залогиниться в докер под гитлабовской учёткой:
```bash
sudo docker login registry.gitlab.com/woksystem/parser -u <username> -p <password>
```
3. Загрузить докер образ с репы гитлаба:
```bash
sudo docker pull registry.gitlab.com/woksystem/parser:latest
```
4. Запустить докер контейнер:
```bash
sudo docker run -p 8080:80 -e PYTHONUNBUFFERED=1 registry.gitlab.com/woksystem/parser:latest
```
## Запуск на локальном пк
1. Установить [docker-ce](https://docs.docker.com/install/linux/docker-ce/debian/)
2. Из директории проекта собрать докер образ:
```bash
sudo docker build -t <image_name> .
```
3. Запустить докер контейнер:
```bash
sudo docker run -p 8080:80 -e PYTHONUNBUFFERED=1 <image_name>
```
## Запросы
### ~~Новости~~
- ~~Обновить на сервере - пустой POST на http://`{ip}`:8080/news~~
- ~~Получить - GET на http://`{ip}`:8080/news~~

~~При получении можно указать заголовки:~~
- ~~Page - номер страницы с новостями(стандарт 1)~~
- ~~Per_page - количество новостей на странице(стандарт 20)~~

### Расписание
- Получить расписание на одну группу - GET на http://`{ip}`:8080/schedule?group=`{group}`&hash=`{hash}` (параметр `{group}` обязательный, `{hash}` нет)
- Получить список групп - GET на http://`{ip}`:8080/groups?hash=`{hash}` (параметр `{hash}` необязательный)