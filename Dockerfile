FROM nginx

EXPOSE 80

RUN apt update && apt install -y python3-pip supervisor
COPY requirements.txt .
RUN pip3 install -r requirements.txt
RUN mkdir -p /root/bin/data/groups

COPY etc/nginx.conf /etc/nginx/
COPY etc/supervisord.conf /etc/supervisor/conf.d/server.conf
COPY src /root/bin

CMD ["/usr/bin/supervisord", "-n"]
