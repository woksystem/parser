# -*- coding: utf-8 -*-
import pytest

from src.schedule_parser import convert_type
from src.schedule_parser import day_json
from src.schedule_parser import make_schedule
from src.schedule_parser import parse_csv_schedule

from src.packages.misc import readFile
from src.packages.misc import readJson

class TestScheduleParser:

    def test_parse_csv(self):
        test_groups = set(["ЮРУб-18-2"])
        test_schedules = readJson("tests/test_data/test_schedule_gold.json")
        schedule_csv = readFile("tests/test_data/test_schedule.txt")
        assert(schedule_csv != None)

        groups, schedules = parse_csv_schedule(schedule_csv)
        assert(groups == test_groups)
        assert(schedules["ЮРУб-18-2"] == test_schedules)

    def test_convert_type(self):
        assert(convert_type("1. Лекция") == "Лекция")
        assert(convert_type("2. Практ.") == "Практика")
        assert(convert_type("3. Лаб. раб.") == "Лабораторная работа")
        assert(convert_type("4. Практ. раб.") == None)

    def test_make_day(self):
        test_day = {
            "isDual": False,
            "f_name": "Математика",
            "s_name": "",
            "t_start": "9:55",
            "t_end": "11:35",
            "f_type": "Лекция",
            "s_type": "",
            "f_teacher": "Королёв А.В.",
            "s_teacher": "",
            "f_location": "A100",
            "s_location": ""
        }

        day = day_json(False, "Математика", "",
            "9:55", "11:35", "Лекция", "",
            "Королёв А.В.", "", "A100", "")
        assert(day == test_day)

    def test_make_schedule(self):
        test_schedule = {
            'schedule': {
                'period': "2019-10-10 - 2019-12-28",
                'days': [
                    {'name': 'Понедельник', 'isEven': True, 'lessons': []},
                    {'name': 'Понедельник', 'isEven': False, 'lessons': []},
                    {'name': 'Вторник', 'isEven': True, 'lessons': []},
                    {'name': 'Вторник', 'isEven': False, 'lessons': []},
                    {'name': 'Среда', 'isEven': True, 'lessons': []},
                    {'name': 'Среда','isEven': False,'lessons': []},
                    {'name': 'Четверг','isEven': True,'lessons': []},
                    {'name': 'Четверг','isEven': False,'lessons': []},
                    {'name': 'Пятница','isEven': True,'lessons': []},
                    {'name': 'Пятница','isEven': False,'lessons': []},
                    {'name': 'Суббота','isEven': True,'lessons': []},
                    {'name': 'Суббота','isEven': False,'lessons': []}
                ]
            }
        }
        schedule = make_schedule("2019-10-10 - 2019-12-28")
        assert(schedule == test_schedule)